import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class prorechner {
    private JPanel ProRechner;
    private JTextField Display;
    private JButton cButton;
    private JButton a7Button;
    private JButton a4Button;
    private JButton a1Button;
    private JButton πButton;
    private JButton button11;
    private JButton a8Button;
    private JButton a5Button;
    private JButton a2Button;
    private JButton a0Button;
    private JButton DELETEButton1;
    private JButton a9Button;
    private JButton a6Button;
    private JButton a3Button;
    private JButton button5;
    private JButton DELETEButton;
    private JButton button17;
    private JButton button18;
    private JButton button19;
    private JButton button20;
    private JButton button1;
    private JButton xButton;
    private JButton xⁿButton;
    private JButton ⁿButton;
    private JButton logButton;
    private JButton sinButton;
    private JButton cosButton;
    private JButton tanButton;

    double now, now2, result;
    String operator;

    public prorechner() {
        cButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText("");
            }
        });
        a7Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               Display.setText(Display.getText() + a7Button.getText());
            }
        });
        a8Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a8Button.getText());
            }
        });
        a9Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a9Button.getText());
            }
        });
        a6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a6Button.getText());
            }
        });
        a5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a5Button.getText());
            }
        });
        a4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a4Button.getText());
            }
        });
        a3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a3Button.getText());
            }
        });
        a2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a2Button.getText());
            }
        });
        a1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a1Button.getText());
            }
        });
        a0Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + a0Button.getText());
            }
        });
        button11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double vorzeichen = Double.parseDouble(Display.getText());
                vorzeichen = vorzeichen *-1;
                Display.setText(String.valueOf(vorzeichen));

            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!Display.getText().contains(".")) {
                    Display.setText(Display.getText() + button5.getText());
                }
            }
        });
        DELETEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                now = Double.parseDouble(Display.getText());
                operator = "+";
                Display.setText("");
            }
        });
        button17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                now = Double.parseDouble(Display.getText());
                operator = "-";
                Display.setText("");
            }
        });
        button18.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                now = Double.parseDouble(Display.getText());
                operator = "/";
                Display.setText("");
            }
        });
        button19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                now = Double.parseDouble(Display.getText());
                operator = "*";
                Display.setText("");
            }
        });
        DELETEButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Display.getText().length() > 0){
                    StringBuilder sb = new StringBuilder(Display.getText());
                    sb.deleteCharAt(Display.getText().length() - 1);
                    Display.setText(String.valueOf(sb));
                }
            }
        });
        button20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {

                    now2 = Double.parseDouble(Display.getText());

                    if (operator == "+") {
                        result = now + now2;

                    } else if (operator == "-") {
                        result = now - now2;
                    } else if (operator == "/") {
                        result = now / now2;
                    } else if (operator == "*") {
                        result = now * now2;
                    } else if (operator == "^") {
                        result = Math.pow(now, now2);
                    } else if (operator == "W") {
                        result = Math.pow(now, 1/now2);
                    }


                    Display.setText(String.valueOf(result));


            }
        });
        πButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(Display.getText() + Math.PI);
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double z = Double.parseDouble(Display.getText());
                Display.setText(String.valueOf(Math.sqrt(z)));

            }
        });
        xButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double hochzahl = Math.pow(Double.parseDouble(Display.getText()), 2);
                Display.setText(String.valueOf(hochzahl));
            }
        });
        xⁿButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                now = Double.parseDouble(Display.getText());
                operator = "^";
                Display.setText("");
            }
        });
        ⁿButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                now = Double.parseDouble(Display.getText());
                operator = "W";
                Display.setText("");
            }
        });
        logButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(String.valueOf(Math.log10(Double.parseDouble(Display.getText()))));
            }
        });
        sinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(String.valueOf(Math.sin(Math.toRadians(Double.parseDouble(Display.getText())))));
            }
        });
        cosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(String.valueOf(Math.cos(Math.toRadians(Double.parseDouble(Display.getText())))));
            }
        });
        tanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Display.setText(String.valueOf(Math.tan(Math.toRadians(Double.parseDouble(Display.getText())))));
            }
        });
    }



    public static void main(String[] args) {

            JFrame frame = new JFrame("prorechner");
            frame.setContentPane(new prorechner().ProRechner);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);

    }

}
