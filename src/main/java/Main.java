import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        double zahl1;
        double zahl2;
        char operator;
        double result = 0;

        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Geben sie die Rechnung ein: ");
            zahl1 = scanner.nextInt();

            operator = scanner.next().charAt(0);

            zahl2 = scanner.nextInt();

            if (operator == '+') {
                result = zahl1 + zahl2;
            } else if (operator == '-') {
                result = zahl1 - zahl2;
            } else if (operator == '/' || operator == ':') {
                result = zahl1 / zahl2;
            } else if (operator == '*' || operator == 'x') {
                result = zahl1 * zahl2;
            } else {
                System.out.println("Unbekannter Operator");
            }

            System.out.println("Das Ergebnis deiner Rechnung ist: " + result);
        }

    }
}
